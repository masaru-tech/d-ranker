import React, { PropTypes } from 'react';
import {new_sake_evaluation_path} from '../rails-routes';

export default class SakeItem extends React.Component {
  // static propTypes = {
  //   id: PropTypes.number.isRequired,
  //   name: PropTypes.string.isRequired,
  //   yomi: PropTypes.string.isRequired,
  // };

  render() {
    let {
      sake,
      style
    } = this.props
    return (
      <a href={new_sake_evaluation_path({sake_id: sake.id})}>
        <article className="media sake-item" style={style}>
          <div className="media-content">
            <div className="content">
              <p>
                <strong>{sake.name}</strong>
                <br />
                {sake.yomi}
              </p>
            </div>
          </div>
        </article>
      </a>
    );
  }
}
