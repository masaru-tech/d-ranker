import React, { PropTypes } from 'react';

export default class HelloWorldWidget extends React.Component {
  static propTypes = {
    service_name: PropTypes.string.isRequired,
    link_text: PropTypes.string.isRequired,
  };

  render() {
    const faClass = "fa fa-" + this.props.service_name
    const href = "/auth/" + this.props.service_name
    return (
      <div>
        <a className="button is-primary" href={href}>
          <span className="icon">
            <i className={faClass}></i>
          </span>
          <span>{this.props.link_text}</span>
        </a>
      </div>
    );
  }
}
