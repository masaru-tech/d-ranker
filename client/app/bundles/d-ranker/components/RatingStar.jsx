import React, { PropTypes } from 'react';

export default class RatingStar extends React.Component {
  constructor (props, context) {
    super(props, context)

    this.handleRadio = this.props.handleRadio.bind(this)
  }
  render() {
    return (
      <span className="star-rating">
        <input type="radio" name="rating" value="1" onChange={this.handleRadio} /><i></i>
        <input type="radio" name="rating" value="2" onChange={this.handleRadio} /><i></i>
        <input type="radio" name="rating" value="3" onChange={this.handleRadio} /><i></i>
        <input type="radio" name="rating" value="4" onChange={this.handleRadio} /><i></i>
        <input type="radio" name="rating" value="5" onChange={this.handleRadio} /><i></i>
      </span>
    );
  }
}
