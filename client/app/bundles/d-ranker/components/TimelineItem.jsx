import React, { PropTypes } from 'react';
import RatingStar from './RatingStar';

export default class TimelineItem extends React.Component {
  render() {
    const {
      timeline,
      style
    } = this.props

    return (
      <a href="/home/1">
        <article className="media timeline-item " style={style}>
          <div className="media-left">
            <figure className="image is-64x64">
              <img src="http://placehold.it/128x128" style={{borderRadius: '64px'}} />
            </figure>
          </div>
          <div className="media-content">
            <div className="content">
              <strong>{timeline.username}</strong> <small>{timeline.drunk_at}</small>
              <br />
              <b>『{timeline.alcohol_name}』</b>を呑んだー
              <br />
              <span className={`rate rate${timeline.rating}`}></span>
            </div>
          </div>
        </article>
      </a>
    );
  }
}