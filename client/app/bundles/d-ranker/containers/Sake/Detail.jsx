import React, { PropTypes } from 'react';
import RatingStar from '../../components/RatingStar';
import {sake_evaluations_path} from '../../rails-routes';
import axios from 'axios';

export default class Detail extends React.Component {
  constructor (props, context) {
    super(props, context)

    this.handleRadio = this.handleRadio.bind(this)
    this.onSubmit = this.onSubmit.bind(this)
    this.state = {
      rating: null
    }
  }

  handleRadio(e) {
    this.setState({rating: e.currentTarget.value})
  }

  onSubmit() {
    const csrfToken = ReactOnRails.authenticityToken();
    const that = this;
    axios.post(sake_evaluations_path({sake_id: this.props.sake.id}), {
          authenticity_token: csrfToken,
          rating: this.state.rating
        })
        .then((response) => {
          console.log(response)
          webkit.messageHandlers.closeAction.postMessage("Hello!")
        })
        .catch((error) => {
          console.log(error);
        });
  }

  render() {
    const {
      sake
    } = this.props

    return (
      <form action={sake_evaluations_path({sake_id: sake.id})} method="post">
        <div style={{padding: '10px'}}>
          <div className="card is-fullwidth">
            <header className="card-header">
              <p className="card-header-title">
                {sake.name}
              </p>
              <a className="card-header-icon">
                <i className="fa fa-angle-down"></i>
              </a>
            </header>
            <div className="card-content" style={{padding: '0px'}}>
              <div className="ios-table-view">
                <ul>
                  <li className="section-title" style={{fontSize: '13px'}}>読み</li>
                  <li>
                    <span>
                      {sake.yomi}
                    </span>
                  </li>
                </ul>
                <ul>
                  <li className="section-title" style={{fontSize: '13px'}}>蔵元名</li>
                  <li>
                    <span>
                      {sake.kuramoto}
                    </span>
                  </li>
                </ul>
                <ul>
                  <li className="section-title" style={{fontSize: '13px'}}>都道府県</li>
                  <li>
                    <span>
                      {sake.locality}
                    </span>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>

        <div className="ios-table-view">
          <ul>
            <li className="section-title">お気に入り度</li>
            <li style={{textAlign: 'center'}}>
              <RatingStar handleRadio={this.handleRadio} />
            </li>
          </ul>
          <ul>
            <li className="section-title">場所</li>
            <li><a href="">場所を選択(任意)</a></li>
          </ul>
          <ul>
            <li className="section-title">画像</li>
            <li><a href="">画像を選択(任意)</a></li>
          </ul>
          <ul>
            <li className="section-title">メモ</li>
            <li>
              <textarea className="textarea" placeholder="メモを記入(任意)"></textarea>
            </li>
          </ul>
        </div>
        <button className="button is-primary is-fullwidth" type='button' name='action' value='send' onClick={this.onSubmit}>送信</button>
      </form>
    );
  }
}