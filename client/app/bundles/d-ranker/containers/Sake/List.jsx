import React, { Component,PropTypes } from 'react';
import SakeItem from '../../components/SakeItem';
import { List, InfiniteLoader, CellMeasurer } from 'react-virtualized';
import axios from 'axios';
import {sakes_path} from '../../rails-routes';

export default class ListExample extends Component {
  constructor (props, context) {
    super(props, context)

    this.state = {
      sakes: props.sakes,
      value: ''
    }

    this._isRowLoaded = this._isRowLoaded.bind(this)
    this._loadMoreRows = this._loadMoreRows.bind(this)
    this._rowRenderer = this._rowRenderer.bind(this)
    this.handleChange = this._handleChange.bind(this)
  }

  render () {
    return (
      <div>
        <input type="text" className="input" value={this.state.value} placeholder="お酒名/読みで検索" onChange={this.handleChange} />
        <InfiniteLoader
          isRowLoaded={this._isRowLoaded}
          loadMoreRows={this._loadMoreRows}
          rowCount={this.state.sakes.length}
        >
          {({ onRowsRendered, registerChild }) => (
            <CellMeasurer
              cellRenderer={
                ({ rowIndex, ...rest }) => this._rowRenderer({ index: rowIndex, ...rest })
              }
              columnCount={1}
              rowCount={this.state.sakes.length}
            >
              {({ getRowHeight, resetMeasurements }) => (
                <List
                  ref={registerChild}
                  height={screen.availHeight - 44 - 20 - 32} // NavigationBar(44) + StatusBar(20)の高さ + 検索用input(32)の高さ
                  overscanRowCount={10}
                  rowCount={this.state.sakes.length}
                  onRowsRendered={onRowsRendered}
                  rowHeight={getRowHeight}
                  rowRenderer={
                    ({index, ...rest}) => {
                      resetMeasurements()
                      return this._rowRenderer({ index: index, ...rest })
                    }
                  }
                  width={screen.availWidth}
                />
              )}
            </CellMeasurer>
          )}
        </InfiniteLoader>
      </div>
    )
  }

  _isRowLoaded ({ index }) {
    return index + 1 < this.state.sakes.length
  }

  _loadMoreRows ({ startIndex, stopIndex }) {
    const that = this;
    let page = Math.floor(this.state.sakes.length / 20) + 1
    const keyword = this.state.value;
    axios.get(`${sakes_path()}.json?page=${page}&keyword=${keyword}`)
        .then((response) => {
          that.setState({
            sakes: that.state.sakes.concat(response.data)
          })
        })
        .catch((error) => {
          console.log(error);
        });
  }

  _rowRenderer ({ index, isScrolling, key, style }) {
    return (
      <SakeItem sake={this.state.sakes[index]} key={key} style={style} />
    )
  }

  _handleChange(event) {
    const that = this;
    const keyword = event.target.value;
    this.setState({value: keyword, sakes: []});
    axios.get(sakes_path() + '.json?keyword='+ keyword)
        .then((response) => {
          that.setState({
            sakes: that.state.sakes.concat(response.data)
          })
        })
        .catch((error) => {
          console.log(error);
        });
  }
}