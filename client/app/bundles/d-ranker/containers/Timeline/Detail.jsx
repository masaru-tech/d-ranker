import React, { PropTypes } from 'react';
import RatingStar from '../../components/RatingStar';

export default class Detail extends React.Component {
  render() {
    return (
      <div>
        <div style={{padding: '10px'}}>
          <div className="card is-fullwidth">
            <header className="card-header">
              <p className="card-header-title">
                お酒名
              </p>
            </header>
            <div className="card-content">
              <div className="content">
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus nec iaculis mauris. <a href="#">@bulmaio</a>. <a href="#">#css</a> <a href="#">#responsive</a>
                <br />
                <small>11:09 PM - 1 Jan 2016</small>
              </div>
            </div>
            <footer className="card-footer">
              <p style={{flexGrow: '1', padding: '5px 10px'}}>
                詳細表示
              </p>
              <a className="card-header-icon">
                <i className="fa fa-angle-down"></i>
              </a>
            </footer>
          </div>
        </div>

        <div className="ios-table-view">
          <ul>
            <li className="section-title">お気に入り度</li>
            <li style={{textAlign: 'center'}}>
              <RatingStar />
            </li>
          </ul>
          <ul>
            <li className="section-title">場所</li>
            <li><a href="usage.html">場所を選択</a></li>
          </ul>
          <ul>
            <li className="section-title">画像</li>
            <li><a href="contact.html">画像を選択</a></li>
          </ul>
          <ul>
            <li className="section-title">メモ</li>
            <li>
              <textarea className="textarea" placeholder="メモを記入"></textarea>
            </li>
          </ul>
        </div>
      </div>
    );
  }
}