import React, { PropTypes } from 'react';
import TimelineItem from '../../components/TimelineItem';
import { List, InfiniteLoader, CellMeasurer } from 'react-virtualized';
import axios from 'axios';
import {home_index_path} from '../../rails-routes';

// Simple example of a React "smart" component
export default class Timeline extends React.Component {
  constructor (props, context) {
    super(props, context)

    this.state = {
      timeline: props.timeline,
      current_page: 1
    }

    this._isRowLoaded = this._isRowLoaded.bind(this)
    this._loadMoreRows = this._loadMoreRows.bind(this)
    this._rowRenderer = this._rowRenderer.bind(this)
  }

  render() {
    return (
      <div>
        <InfiniteLoader
          isRowLoaded={this._isRowLoaded}
          loadMoreRows={this._loadMoreRows}
          rowCount={this.state.timeline.length}
        >
          {({ onRowsRendered, registerChild }) => (
            <CellMeasurer
              cellRenderer={
                ({ rowIndex, ...rest }) => this._rowRenderer({ index: rowIndex, ...rest })
              }
              columnCount={1}
              rowCount={this.state.timeline.length}
            >
              {({ getRowHeight, resetMeasurements }) => (
                <List
                  ref={registerChild}
                  height={screen.availHeight - 44 - 20 - 49} // NavigationBar(44) + StatusBar(20)の高さ + Tab Barの高さ(49)
                  overscanRowCount={10}
                  rowCount={this.state.timeline.length}
                  onRowsRendered={onRowsRendered}
                  rowHeight={getRowHeight}
                  rowRenderer={
                    ({index, ...rest}) => {
                      resetMeasurements()
                      return this._rowRenderer({ index: index, ...rest })
                    }
                  }
                  width={screen.availWidth}
                />
              )}
            </CellMeasurer>
          )}
        </InfiniteLoader>
      </div>
    );
  }

  _isRowLoaded ({ index }) {
    return index + 1 < this.state.timeline.length
  }

  _loadMoreRows ({ startIndex, stopIndex }) {
    const that = this;
    let page = Math.floor(this.state.timeline.length / 20) + 1

    if (this.state.current_page == page){
      return
    }

    axios.get(`${home_index_path()}.json?page=${page}`)
        .then((response) => {
          that.setState({
            timeline: that.state.timeline.concat(response.data),
            current_page: page
          })
        })
        .catch((error) => {
          console.log(error);
        });
  }

  _rowRenderer ({ index, isScrolling, key, style }) {
    return (
      <TimelineItem timeline={this.state.timeline[index]} key={key} style={style} />
    )
  }
}