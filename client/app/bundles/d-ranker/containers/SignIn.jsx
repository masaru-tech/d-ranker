import React, { PropTypes } from 'react';
import SocialLoginButton from '../components/SocialLoginButton';

// Simple example of a React "smart" component
export default class SingIn extends React.Component {
  render() {
    return (
      <section className="hero">
        <div className="hero-body">
          <div className="container has-text-centered">
            <h1 className="title">
              ログイン
            </h1>
            <div>
              <SocialLoginButton service_name="twitter" link_text="Twitterでログインする" />
            </div>
          </div>
        </div>
      </section>
    );
  }
}