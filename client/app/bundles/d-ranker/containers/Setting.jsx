import React, { PropTypes } from 'react';
import ReactOnRails from 'react-on-rails';
import {sign_out_path} from '../rails-routes';

// Simple example of a React "smart" component
export default class Setting extends React.Component {
  render() {
    const csrfToken = ReactOnRails.authenticityToken()
    return (
      <div>
        <div className="card is-fullwidth profile">
          <header className="card-header">
          </header>
          <div className="card-content">
            <a className="card-avatar">
              <img src="" className="card-avatar-img" />
            </a>

            <div className="card-user">
              <div className="pull-left">
                <div className="card-user-name">
                  <a href="#">John Smith</a>
                </div>
                <span>
                  <a href="#">@jsmith</a>
                </span>
              </div>
              <div className="pull-right" style={{paddingRight: '5px'}}>
                <form method="post" action={sign_out_path()}>
                  <input type="hidden" name="_method" value="delete" />
                  <input type="submit" className="button" value="ログアウト" />
                  <input type="hidden" name="authenticity_token" value={csrfToken} />
                </form>
              </div>
            </div>

            <div className="card-stats">
              <ul className="card-stats-list">
                <li className="card-stats-item">
                  <a href="#" title="9.840 Tweet">
                    <span className="card-stats-key">飲んだー</span>
                    <span className="card-stats-val">1</span>
                  </a>
                </li>
                <li className="card-stats-item">
                  <a href="#/following" title="885 Following">
                    <span className="card-stats-key">フォロー</span>
                    <span className="card-stats-val">0</span>
                  </a>
                </li>
                <li className="card-stats-item">
                  <a href="#">
                    <span className="card-stats-key">フォロワー</span>
                    <span className="card-stats-val">0</span>
                  </a>
                </li>
              </ul>
            </div>
          </div>
        </div>
        <div>
          <a href="/home/1">
            <article className="media" style={{padding: '10px', marginTop: '0', borderBottom: '1px solid #dbdbdb'}}>
              <div className="media-left">
                <figure className="image is-64x64">
                  <img src="http://placehold.it/128x128" style={{borderRadius: '64px'}} />
                </figure>
              </div>
              <div className="media-content">
                <div className="content">
                  <p>
                    <strong>John Smith</strong> <small>2016/10/12</small>
                    <br />
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean efficitur sit amet massa fringilla egestas. Nullam condimentum luctus turpis.
                  </p>
                </div>
              </div>
            </article>
          </a>
          <a href="/home/1">
            <article className="media" style={{padding: '10px', marginTop: '0', borderBottom: '1px solid #dbdbdb'}}>
              <div className="media-left">
                <figure className="image is-64x64">
                  <img src="http://placehold.it/128x128" style={{borderRadius: '64px'}} />
                </figure>
              </div>
              <div className="media-content">
                <div className="content">
                  <p>
                    <strong>John Smith</strong> <small>2016/10/12</small>
                    <br />
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean efficitur sit amet massa fringilla egestas. Nullam condimentum luctus turpis.
                  </p>
                </div>
              </div>
            </article>
          </a>
          <a href="/home/1">
            <article className="media" style={{padding: '10px', marginTop: '0', borderBottom: '1px solid #dbdbdb'}}>
              <div className="media-left">
                <figure className="image is-64x64">
                  <img src="http://placehold.it/128x128" style={{borderRadius: '64px'}} />
                </figure>
              </div>
              <div className="media-content">
                <div className="content">
                  <p>
                    <strong>John Smith</strong> <small>2016/10/12</small>
                    <br />
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean efficitur sit amet massa fringilla egestas. Nullam condimentum luctus turpis.
                  </p>
                </div>
              </div>
            </article>
          </a>
        </div>
      </div>
    );
  }
}