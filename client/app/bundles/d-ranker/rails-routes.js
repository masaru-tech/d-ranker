// Don't edit manually. `rake js:routes` generates this file.
export function auth_twitter_callback_path(params) { return '/auth/twitter/callback'; }
export function edit_sake_evaluation_path(params) { return '/sakes/' + params.sake_id + '/evaluations/' + params.id + '/edit'; }
export function home_index_path(params) { return '/home/index'; }
export function new_sake_evaluation_path(params) { return '/sakes/' + params.sake_id + '/evaluations/new'; }
export function rails_info_path(params) { return '/rails/info'; }
export function rails_info_properties_path(params) { return '/rails/info/properties'; }
export function rails_info_routes_path(params) { return '/rails/info/routes'; }
export function rails_mailers_path(params) { return '/rails/mailers'; }
export function root_path(params) { return '/'; }
export function sake_evaluation_path(params) { return '/sakes/' + params.sake_id + '/evaluations/' + params.id + ''; }
export function sake_evaluations_path(params) { return '/sakes/' + params.sake_id + '/evaluations'; }
export function sakes_path(params) { return '/sakes'; }
export function sessions_path(params) { return '/sessions'; }
export function settings_path(params) { return '/settings'; }
export function sign_in_path(params) { return '/sign_in'; }
export function sign_out_path(params) { return '/sign_out'; }
