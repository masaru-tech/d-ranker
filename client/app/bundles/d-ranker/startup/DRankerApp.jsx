import React from 'react';
import ReactOnRails from 'react-on-rails';

import SignIn from '../containers/SignIn';
import Timeline from '../containers/Timeline/index';
import TimelineDetail from '../containers/Timeline/Detail';
import SakeList from '../containers/Sake/List';
import SakeDetail from '../containers/Sake/Detail';
import Setting from '../containers/Setting';

ReactOnRails.register({
  SignIn,
  Timeline,
  TimelineDetail,
  SakeList,
  SakeDetail,
  Setting
});
