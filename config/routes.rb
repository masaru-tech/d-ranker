Rails.application.routes.draw do
  get 'home/index'
  get 'home/:id' => 'home#show'

  # get 'hello_world', to: 'hello_world#index'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  get 'sign_in' => 'sessions#new'
  post 'sessions' => 'sessions#create'
  delete 'sign_out' => 'sessions#destroy'

  [:twitter].each do |provider|
    get "auth/#{provider}/callback" => "omniauth_callbacks##{provider}"
  end

  resources :sakes, only: [:index] do
    resources :evaluations, except: [:index]
  end

  resources :settings, only: [:index]

  root 'home#index'
end
