FROM ruby:2.3

ENV APP_ROOT /app

RUN apt-get update && apt-get install -y nodejs npm postgresql-client --no-install-recommends && rm -rf /var/lib/apt/lists/*
RUN npm cache clean && npm install n -g && n 7.2.1 && ln -sf /usr/local/bin/node /usr/bin/node && npm update -g npm
RUN apt-get purge -y nodejs

RUN mkdir -p $APP_ROOT
WORKDIR $APP_ROOT

COPY Gemfile $APP_ROOT
COPY Gemfile.lock $APP_ROOT
RUN bundle install -j4 --without development test

COPY . $APP_ROOT

RUN cd client && npm install
RUN bundle exec rails assets:precompile RAILS_ENV=production

EXPOSE 3000
CMD ["bundle", "exec", "puma", "-C", "config/puma.rb"]
