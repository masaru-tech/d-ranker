class EvaluationsController < ApplicationController
  def new
    alcohol = Alcohol.find(params[:sake_id])
    @alcohol_with_info = ::Resources::Sake.new(alcohol).as_json
  end

  def create
    alcohol = Alcohol.find(params[:sake_id])
    Evaluation.create!({
        alcohol_id: alcohol.id,
        user_id: current_user.id,
        rating: params[:rating]
      })

    render nothing: true, status: 201
  end
end
