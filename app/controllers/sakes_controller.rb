class SakesController < ApplicationController
  def index
    if params[:keyword].blank?
      sakes = Alcohol.all.order(:id).page(params[:page]).per(20)
    else
      sakes = Alcohol.where('name like :keyword or yomi like :keyword_kata',
                {keyword: "%#{params[:keyword]}%", keyword_kata: "%#{Moji.kata_to_hira(params[:keyword])}%"})
                .order(:id).page(params[:page]).per(20)
    end
    @sakes = sakes.map{|sake| ::Resources::Sake.new(sake).as_json(only: [:id, :name, :yomi])}

    respond_to do |format|
      format.html
      format.json { render json: @sakes }
    end
  end
end
