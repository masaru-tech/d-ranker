require "application_responder"

class ApplicationController < ActionController::Base
  self.responder = ApplicationResponder
  respond_to :html

  include Monban::ControllerHelpers

  protect_from_forgery with: :exception

  before_action :require_login
end
