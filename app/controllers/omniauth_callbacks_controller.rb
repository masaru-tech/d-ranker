class OmniauthCallbacksController < ApplicationController
  skip_before_action :require_login

  def twitter
    profile = SocialProfile.find_or_initialize_for_oauth(request.env['omniauth.auth'])
    user = profile.user

    unless user
      user = User.new(
        username: request.env['omniauth.auth']["info"]["name"]
      )
      user.save(validate: false)
      profile.user = user
      profile.save
    end

    sign_in(user)

    redirect_to home_index_path
  end
end
