class HomeController < ApplicationController
  def index
    evaluations = Evaluation.where(user_id: current_user.id).preload(:alcohol, :user)
                            .order('created_at desc').page(params[:page]).per(20)

    @evaluations = evaluations.map{|evaluation| ::Resources::Evaluation.new(evaluation).as_json}

    respond_to do |format|
      format.html
      format.json { render json: @evaluations }
    end
  end

  def show
  end
end
