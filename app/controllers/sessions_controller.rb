class SessionsController < ApplicationController
  skip_before_action :require_login

  def new
  end

  def destroy
    sign_out

    redirect_to sign_in_path
  end
end
