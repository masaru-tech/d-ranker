module Resources
  class Evaluation < Base

    title 'Evaluation'
    property(
      :id,
      description: '評価のid',
      example: 1,
      type: Integer
    )

    property(
      :rating,
      description: '評価の数値',
      example: 4,
      type: Integer
    )

    property(
      :drunk_at,
      description: '評価した日時',
      example: "2016/12/11 19:25:11",
      type: String
    )

    property(
      :alcohol_name,
      description: 'お酒の名前',
      example: "獺祭 純米大吟醸 磨き二割三分 遠心分離",
      type: String
    )

    property(
      :username,
      description: "ユーザ名",
      example: "masaruTech",
      type: String
    )

    delegate(
      :id,
      :rating,
      :drunk_at,
      to: :model
    )

    def drunk_at
      model.created_at.strftime('%Y/%m/%d %H:%M')
    end

    def method_missing(name, *arguments)
      case 
      when [:alcohol_name].include?(name)
        model.alcohol.__send__ name.to_s.gsub('alcohol_', '').to_sym, *arguments
      when [:username].include?(name)
        model.user.__send__ name.to_s.gsub('user_', '').to_sym, *arguments
      else
        super
      end
    end
  end
end
