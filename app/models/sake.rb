class Sake < Alcohol
  has_one :sake_info, foreign_key: :alcohol_id
end
